'use strict'

class Phone {

    constructor() {
        this.base_url = 'http://localhost:3000';
        this.items = [];
        this.confirmed = false;
    }

    buildUrlForGet(endpoint, params) {
        let dc = Math.random(Number.MAX_SAFE_INTEGER).toString();
        let prm = '';
        if (!!params) {
            prm = '&' + params;
        }
        return this.base_url + endpoint + '?_dc=' + dc + prm;
    }

    loadItems() {
        var me = this;
        return new Promise(function (resolve, reject) {
            let url = me.buildUrlForGet('/items');
            var request = new XMLHttpRequest();
            request.open('GET', url);
            request.responseType = 'json';
            request.onload = function () {
                if (request.status === 200) {
                    resolve(request.response);
                } else {
                    reject(Error('Items didn\'t load successfully; error code:' + request.statusText));
                }
            };
            request.onerror = function () {
                reject(Error('There was a network error'));
            };
            request.send();
        });
    }

    postSelectItem(value) {
        var me = this;
        return new Promise(function (resolve, reject) {
            let url = me.buildUrlForGet('/select/' + value);
            var request = new XMLHttpRequest();
            request.open('POST', url);
            request.responseType = 'text';
            request.onload = function () {
                if (request.status === 200) {
                    resolve(request.response);
                } else {
                    reject(Error('Items didn\'t load successfully; error code:' + request.statusText));
                }
            };
            request.onerror = function () {
                reject(Error('There was a network error'));
            };
            request.send();
        });
    }

    getBtnRefresh() {
        return document.getElementById('btn-refresh');
    }

    getBtnSelect() {
        return document.getElementById('btn-select');
    }

    getBtnConfirm() {
        return document.getElementById('confirm');
    }

    getPhoneSelect() {
        return document.getElementById('phone-select');
    }

    fillSelect(items) {
        let phoneSelect = this.getPhoneSelect();
        phoneSelect.innerHTML = '';
        for (var i = 0; i < items.length; i++) {
            phoneSelect.innerHTML = phoneSelect.innerHTML +
                '<option value="' + items[i]['id'] + '">' + items[i]['name'] + '</option>';
        }
    }

    init() {
        const me = this;
        const btnRefresh = this.getBtnRefresh();
        const btnSelect = this.getBtnSelect();
        const btnConfirm = this.getBtnConfirm();

        btnRefresh.onclick = function () {
            btnRefresh.disabled = true;
            me.loadItems()
                .then((result) => {
                    me.fillSelect(result);
                })
                .finally(() => {
                    btnRefresh.disabled = false;
                })
        };
        btnConfirm.onchange = function (event) {
            me.confirmed = !event.target.checked;
            btnSelect.disabled = !me.confirmed;
        };
        btnSelect.onclick = function () {
            btnSelect.disabled = true;
            const phoneSelect = document.getElementById("phone-select");
            const selected = phoneSelect.value;
            me.postSelectItem(selected)
                .then((result) => {
                    alert(result);
                });
        };
    }
}
